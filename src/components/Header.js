import React from "react";
import { Link } from "react-router-dom";

export default function Header() {
	return (
		<div className="navbar is-dark">
			<div className="navbar-brand">
				<Link to="/" className="navbar-item">
					Admin
				</Link>
				<Link to="/article" className="navbar-item">
					Мэдээ
				</Link>
				<Link to="/category" className="navbar-item">
					Ангилал
				</Link>
				<Link to="/tag" className="navbar-item">
					Шошго
				</Link>
			</div>
		</div>
	);
}
