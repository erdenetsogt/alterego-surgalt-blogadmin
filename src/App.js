import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Header from "./components/Header";
import HomePage from "./pages/Home";
import ArticlePage from "./pages/Article";
import CategoryPage from "./pages/Category";
import TagPage from "./pages/Tag";

function App() {
	return (
		<Router>
			<Header />

			<Switch>
				<Route exact path="/article">
					<ArticlePage />
				</Route>
				<Route exact path="/category">
					<CategoryPage />
				</Route>
				<Route exact path="/tag">
					<TagPage />
				</Route>
				<Route exact path="/">
					<HomePage />
				</Route>
			</Switch>
		</Router>
	);
}

export default App;
