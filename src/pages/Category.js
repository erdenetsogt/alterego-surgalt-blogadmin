import React, { useEffect, useState } from "react";

export default function () {
	const [open, setOpen] = useState(false);
	const [list, setList] = useState([]);
	const [editing, setEditing] = useState();

	useEffect(() => {
		loadList();
	}, []);

	function loadList() {
		fetch("/api/category/list")
			.then((response) => response.json())
			.then((jsonData) => setList(jsonData));
	}

	function showForm() {
		setOpen(true);
		setEditing(null);
	}

	function hideForm() {
		setOpen(false);
	}

	function handleSubmit() {
		loadList();
		hideForm();
	}

	function deleteThis(id) {
		const confirmed = window.confirm("Устгах уу?");
		if (confirmed) {
			fetch("/api/category/delete", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify({ _id: id }),
			})
				.then((response) => response.json())
				.then(() => {
					loadList();
				});
		}
	}

	function editThis(item) {
		setEditing(item);
		setOpen(true);
	}

	return (
		<div className="container" style={{ maxWidth: 700, marginTop: "2rem" }}>
			<div className="is-flex" style={{ justifyContent: "space-between", marginBottom: "2rem" }}>
				<h1 className="title">Ангилал</h1>
				<button className="button is-success" onClick={showForm}>
					Шинэ
				</button>
			</div>

			<table className="table is-fullwidth is-bordered">
				<thead>
					<tr>
						<th style={{ width: 1 }}>ID</th>
						<th>Нэр</th>
						<th style={{ width: 1 }}></th>
					</tr>
				</thead>
				<tbody>
					{list.map((item) => (
						<tr key={item._id}>
							<td>{item._id}</td>
							<td>{item.name}</td>
							<td>
								<div className="buttons" style={{ flexWrap: "nowrap" }}>
									<button className="button is-small is-danger is-light" onClick={() => deleteThis(item._id)}>
										Устгах
									</button>
									<button className="button is-small is-outlined is-info" onClick={() => editThis(item)}>
										Засах
									</button>
								</div>
							</td>
						</tr>
					))}
				</tbody>
			</table>

			{open && <EditForm editing={editing} onClose={hideForm} onSubmit={handleSubmit} />}
		</div>
	);
}

function EditForm({ onClose, onSubmit, editing }) {
	const [name, setName] = useState("");

	useEffect(() => {
		if (editing) {
			setName(editing.name);
		}
	}, [editing]);

	function handleChange(e) {
		setName(e.target.value);
	}

	function submit() {
		if (editing) {
			const form = {
				name: name,
				_id: editing._id,
			};
			fetch("/api/category/update", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(form),
			})
				.then((response) => response.json())
				.then((jsonData) => onSubmit());
		} else {
			const form = {
				name: name,
			};

			fetch("/api/category/create", {
				method: "POST",
				headers: {
					"Content-Type": "application/json",
				},
				body: JSON.stringify(form),
			})
				.then((response) => response.json())
				.then((jsonData) => onSubmit());
		}
	}

	return (
		<div class="modal is-active">
			<div class="modal-background"></div>
			<div class="modal-content" style={{ maxWidth: 400 }}>
				<div className="box">
					<div class="field">
						<label class="label">Нэр</label>
						<div class="control">
							<input class="input" type="text" value={name} onChange={handleChange} />
						</div>
					</div>

					<div className="buttons" style={{ justifyContent: "space-between" }}>
						<button className="button is-light" onClick={onClose}>
							Болих
						</button>
						<button className="button is-success" onClick={submit}>
							Хадгалах
						</button>
					</div>
				</div>
			</div>
			<button class="modal-close is-large" aria-label="close" onClick={onClose}></button>
		</div>
	);
}
